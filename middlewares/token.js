
var app       = require('express')();
var config    = require('../config');
var getToken  = require('../middlewares/getToken');
var passport  = require('passport');
var User      = require('../models/user');
var jwt       = require('jwt-simple');

app.use(function(req, res, next) {
  console.log(passport.authenticate('jwt', {session: false}));
  var token = getToken(req.headers);
  if (token) {
        req.decoded = jwt.decode(token, config.secret);
        next();
    } else {
    return res.status(403).send({
      success: false, 
      msg: 'No token provided.'
    });
  }
});
module.exports = app;