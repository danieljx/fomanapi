// =======================
// Module dependencies ===
// =======================

var express 	= require('express');
var bodyParser 	= require('body-parser');
var cors 		= require('cors');
var mongoose 	= require('mongoose');
var morgan 		= require('morgan');
var jwt    		= require('jsonwebtoken');
var config 		= require('./config');
var passport	  = require('passport');

var app = module.exports = express();
var port = process.env.PORT || 3000;
// =======================
// Middleware ============
// =======================
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.set('superSecret', config.secret);
app.use(morgan('dev'));
app.use(passport.initialize());
require('./middlewares/passport')(passport);
// =======================
// Routes ================
// =======================
var routes = require('./routes');
app.use(config.route,routes);
app.get('/', function(req, res) {
  res.json({ message: 'Welcome to API on foman web services!' });
});

// =====================================================
// Start server if we're not someone else's dependency =
// =====================================================
if (!module.parent) {
  mongoose.connect(config.database, function() {
    app.listen(port, function() {
      console.log('Foman API escuchando en http://localhost:%s/', port);
    });
  });
}