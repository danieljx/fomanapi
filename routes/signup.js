var app = require('express')();
var User = require('../models/user');

app.route('/signup')
	.post(function(req, res) {
		if (!req.body.user || !req.body.userPass) {
			res.json({succes: false, msg: 'Please pass name and password.'});
		} else {
			var newUser = new User({
				user: req.body.user,
				userPass: req.body.userPass,
				userSta: true
			});
			newUser.save(function(err) {
				console.log(err);
				if (err) {
					res.json({succes: false, msg: 'Username already exists.'});
				} else {
					res.json({succes: true, msg: 'Successful created user!'});
				}
			});
		}
	});

module.exports = app;