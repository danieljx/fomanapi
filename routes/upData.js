var app   = require('express')();
var Menu = require('../models/menu');
var Rule = require('../models/rule');
var User = require('../models/user');

var Company = require('../models/company');
var Train = require('../models/training');
var Funct = require('../models/functionary');

app.get('/upData', function(req, res) {
  console.log('up data');
	/*var menu = new Menu({
		menuName: 'Administrador',
		menuSta : true,
		menuRute: '/users',
		menuPosition: 1
	});
	menu.save(function (err1) {
		if (err1) console.log(err1);
		var admin = new Rule({
			ruleName: 'Administrador',
			ruleSta : true,
			ruleMenu: [menu._id],
			ruleModule: []
		});
		admin.save(function (err2) {
			if (err2) console.log(err2);
			var daniel = new User({
				userName: 'Daniel Villanueva',
				user: 'daniel',
				userPass: '123qwe',
				userSta : true,
				userRule: admin._id
			});
			daniel.save(function (err3) {
				if (err3) console.log(err3);
				// thats it!
			});
		});
	});*/

  	var userget = {};
  User.find()
  .exec()
  .then(function(users) {
    var usersFixed = users.map(function(user) {
    	userget = user;
		return user.toJSON();
    });
	var train = new Train({
		trainName: 'Curso Basico de Manipulacion de Alimentos',
		trainSta : true,
		trainHouer: 4,
		userIn: userget._id
	});
	var train1 = new Train({
		trainName: 'Taller de Limpieza y Desinfeccion',
		trainSta : true,
		trainHouer: 2,
		userIn: userget._id
	});
	var train2 = new Train({
		trainName: 'Conservacion de Alimentos y temperaturas',
		trainSta : true,
		trainHouer: 0.5,
		userIn: userget._id
	});

	train.save(function (err1) {
		if (err1) console.log(err1);
	});
	train1.save(function (err1) {
		if (err1) console.log(err1);
	});
	train2.save(function (err1) {
		if (err1) console.log(err1);
	});
	var comp = new Company({
		companyName: 'Polar',
		companySta : true,
		companyNit: '5874698745',
		companyAdress: 'Calle 78 55 63 este',
		companyEmail: 'rrhh@polar.com',
		companyPhone: '+573979876434',
		companyTrain: [train._id,train1._id],
		userIn: userget._id
	});
	comp.save(function (err1) {
		if (err1) console.log(err1);
	});
	var comp1 = new Company({
		companyName: 'Mac Donalds',
		companySta : true,
		companyNit: '569897645',
		companyAdress: 'Calle 78 89 85 8',
		companyEmail: 'rrhh@macdonalds.com',
		companyPhone: '+5735496565',
		companyTrain: [train._id,train1._id,train2._id],
		userIn: userget._id
	});
	comp1.save(function (err1) {
		if (err1) console.log(err1);
	});
	var funct1 = new Funct({
		funcName: 'Daniel Villanueva',
		funcSta : true,
		funcNit: '62646546',
		funcEmail: 'daniel@foman.com',
		funcPhone: '+57305842684',
		funcCompany: comp._id,
		funcTrain: [train._id,train1._id,train2._id],
		userIn: userget._id
	});
	funct1.save(function (err1) {
		if (err1) console.log(err1);
	});
	var funct2 = new Funct({
		funcName: 'Richar Linares',
		funcSta : true,
		funcNit: '6464656',
		funcEmail: 'richard@macdonalds.com',
		funcPhone: '+5730546546546',
		funcCompany: comp1._id,
		funcTrain: [train._id,train1._id],
		userIn: userget._id
	});
	funct2.save(function (err1) {
		if (err1) console.log(err1);
	});
	var funct3 = new Funct({
		funcName: 'Astrid Carolina Herrera',
		funcSta : false,
		funcNit: '65688331556',
		funcEmail: 'astrid@macdonalds.com',
		funcPhone: '+573064623543',
		funcCompany: comp1._id,
		funcTrain: [train._id],
		userIn: userget._id
	});
	funct3.save(function (err1) {
		if (err1) console.log(err1);
	});
	var funct4 = new Funct({
		funcName: 'PePeto',
		funcSta : false,
		funcNit: '65688331556',
		funcEmail: 'pepeto@polar.com',
		funcPhone: '+573064623543',
		funcCompany: comp1._id,
		funcTrain: [train2._id,train1._id],
		userIn: userget._id
	});
	funct4.save(function (err1) {
		if (err1) console.log(err1);
	});
	console.log("ready");
    });
});
module.exports = app;