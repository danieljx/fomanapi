
var app 	= require('express')();
var User 	= require('../models/user');
var jwt		= require('jwt-simple');
var config	= require('../config');

app.route('/authenticate')
	.post(function(req, res) {
		User.findOne({
				user: req.body.user
		}).populate('userRule')
		.populate('userRule.ruleMenu')
		.populate('userRule.ruleModule')
		.exec()
		.then(function(user) {
			if (!user) {
				res.send({
					success: false, 
					msg: 'Authentication failed.'
				});
			} else if(user.userSta){
					user.comparePassword(req.body.userPass, function(err, isMatch) {
						if (isMatch && !err) {
							var token = jwt.encode(user, config.secret);
								delete user.userPass;
							res.json({
								success: true,
								user: user,
								token:  config.name + ' ' + token
							});
						} else {
							res.send({
								success: false, 
								msg: 'Authentication failed.'
							});
						}
					});
			} else {
				res.send({
					success: false,
					msg: 'Authentication Not authorized.'
				});
			}
		}, function(err) {
			console.log('err', err);
		})
	});
module.exports = app;