
var app = require('express')();

//Datos de Prueba
var upData = require('./upData');
app.use(upData);
/**
 * Routes
 */
var login = require('./authenticate');
app.use(login);

var signup = require('./signup');
app.use(signup);

var user = require('./user');
app.use(user);

var funct = require('./functionary');
app.use(funct);


module.exports = app;