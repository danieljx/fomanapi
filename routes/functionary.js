var app   = require('express')();
var token = require('../middlewares/token');
var Funct = require('../models/functionary');

//app.use('/functionaries', token);
app.set('etag', false);
app.get('/functionaries', function(req, res) {
  var filter = req.param('filter') || {};
  Funct.find(filter)
  //.populate('funcCompany')
  //.populate('funcTrain')
  //.populate('funcUser')
  .exec()
  .then(function(functs) {
    var functsFixed = functs.map(function(funct) {
      return funct.toJSON();
    });
    res
    .status(200)
    .set('Content-Type','application/json')
    .json({
      functionaries: functsFixed
    });
  }, function(err) {
    console.log('err', err);
  })
});
app.route('/functionaries/:id?')
  .all(function(req, res, next) {
    console.log(req.method, req.path, req.body);
    res.set('Content-Type','application/json');
    next();
  }).post(function(req, res) {
    var functionaryNew = req.body.functionary;
    Funct.create(functionaryNew)
    .then(function(funct) {
      res
      .status(201)
      .json({
        functionary: funct.toJSON()
      });
    });
  }).get(function(req, res, next) {
    var id = req.params.id;
    if (!id) {
      console.log('no param');
      return next();
    }
    Funct.findById(id)
    .exec()
    .then(function(functx) {
      if(functx) {
        return res
        .send({
            functionary: functx
        });
      } else {
        return res
        .status(400)
        .send({});
      }
    }, function(err) {
      console.log('err', err);
    });
  }).put(function(req, res, next) {
    var id = req.params.id;
    var functionaryUp = req.body.functionary;
    if (!id) {
      return next();
    }
    Funct.update({_id:id}, functionaryUp, function(err, funct, results) {
      console.log(results);
      if (results.ok) {
        return res
        .json({
          functionary: [functionaryUp]
        });
      }
      res
      .status(500)
      .send(err);
    });
  }).delete(function(req, res) {
    var id = req.params.id;
    if (!id) {
      return next();
    }
    Funct.remove({_id:id}, function() {
      res
      .status(204)
      .send();
    });
  });
module.exports = app;