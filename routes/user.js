var app   = require('express')();
var token = require('../middlewares/token');
var User = require('../models/user');

app.use('/users', token);
app.get('/users', function(req, res) {
  console.log('get users');
  var filter = req.body.filter || {};
  User.find(filter, { userPass: 0})
  .populate('userRule')
  .populate('userRule.ruleMenu')
  .populate('userRule.ruleModule')
  .exec()
  .then(function(users) {
    var usersFixed = users.map(function(user) {
      return user.toJSON();
    });
    res
    .status(200)
    .set('Content-Type','application/json')
    .json({
    users: usersFixed
    });
  }, function(err) {
    console.log('err', err);
  })
});
app.route('/users/:id?')
  .all(function(req, res, next) {
    console.log(req.method, req.path, req.body);
    res.set('Content-Type','application/json');
    next();
  }).post(function(req, res) {
    var userNew = req.body.user;
    console.log(userNew);
    User.create(userNew)
    .then(function(user) {
      res
      .status(201)
      .json({
        user: user.toJSON()
      });
    });
  }).get(function(req, res, next) {
    var id = req.params.id;
    if (!id) {
      console.log('no param');
      return next();
    }
    User.findById(id, function(err, user) {
      if (!user) {
        return res
        .status(400)
        .send({});
      }
      res.json({
        users: user
      })
    });
  }).put(function(req, res, next) {
    var id = req.params.id;
    var userUp = req.body.user;
    if (!id) {
      return next();
    }
    User.update({_id:id}, userUp, function(err, user, results) {
      console.log(results);
      if (results.ok) {
        return res
        .json({
          user: [userUp]
        });
      }
      res
      .status(500)
      .send(err);
    });
  }).delete(function(req, res) {
    var id = req.params.id;
    if (!id) {
      return next();
    }
    User.remove({_id:id}, function() {
      res
      .status(204)
      .send();
    });
  });
module.exports = app;