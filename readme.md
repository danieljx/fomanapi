#API Rest de foman
##Foman es una plataforma que funciona para administrar las capacitaciones tomadas por los empleados de alguna empresa.

## Metodos HTTP permitidos

|  Método  |              Descripción               |
| -------- | -------------------------------------- |
| `GET`    | Obtener un recurso o lista de recursos |
| `POST`   | Crear un recurso                       |
| `PUT`    | Actualizar un recurso                  |
| `DELETE` | Eliminar un recurso                    |

## Códigos de Respuesta

| Código |                         Descripción                          |
| ------ | ------------------------------------------------------------ |
| `200`  | Success                                                      |
| `201`  | Success - nuevo recurso creado.                              |
| `204`  | Success - no hay contenido para responder                    |
| `400`  | Bad Request - i.e. su solicitud no se pudo evaluar           |
| `401`  | Unauthorized - usuario no esta autenticado para este recurso |
| `404`  | Not Found - recurso no existe                                |
| `422`  | Unprocessable Entity - i.e. errores de validación            |
| `429`  | Limite de uso excedido, intente mas tarde                    |
| `500`  | Error de servidor                                            |
| `503`  | Servicio no disponible                                       |

## Crear un usuario nuevo

  Solicitud [POST] /users
    {
      "user":{
        "userName": "Rosa",
        "userPass": "123"
      }
    }

  Respuesta

    {
      "user":{
        "id": 123,
        "userName": "Rosa",
        "userPass": "58749"
      }
    }


## Obtener un usuario
  Solicitud GET /users/123

  Respuesta

    {
      "user":{
        "id": 123,
        "userName": "Rosa",
        "userPass": "58749"
      }
    }

## Actualizar un usuario
  Solicitud PUT /users/123

    {
      nota:{
        "userName": "Rosa Elvira",
        "userPass": "58749"
      }
    }

  Respuesta

    {
      nota:{
        "id": 123,
        "userName": "Rosa Elvira",
        "userPass": "58749"
      }
    }

## Eliminar un usuario

  Solicitud DELETE /users/id (204)


## Obtener una lista de usuarios
  Solicitud GET /users/

  Respuesta

    [{
      user:{
        "id": 123,
        "userName": "Rosa Elvira",
        "userPass": "58749"
      }
    },
    {
      user:{
        "id": 12345,
        "userName": "Alfonso Andres",
        "userPass": "96587"
      }
    }]
