var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var UserSchema = new Schema({
    userName: {
    	type: String
    },
    user: {
    	type: String,
    	required: true,
    	index: {
    		unique: true
    	}
    },
    userPass: {
    	type: String,
    	required: true
    },
    userSta: {
        type: Boolean,
        required: true
    },
    userRule: {
        type: Schema.Types.ObjectId,
        ref: 'Rule',
        required: true
    },
    dateIn: {
        type: Date,
        default: Date.now
    }
});
UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('userPass') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.userPass, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.userPass = hash;
                console.log(user.userPass);
                next();
            });
        });
    } else {
        return next();
    }
});
UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.userPass, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};
module.exports = mongoose.model('User', UserSchema);
