var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var ModuleSchema = new Schema({
    modName: {
    	type: String,
    	required: true,
    	index: {
    		unique: true
    	}
    },
    modSta: {
        type: Boolean,
        required: true
    },
    modRute: {
        type: String,
        required: true
    },
  	dateIn: {
  		type: Date,
  		default: Date.now
  	}
});
module.exports = mongoose.model('Module', ModuleSchema);