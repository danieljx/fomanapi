var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var RuleSchema = new Schema({
    ruleName: {
    	type: String,
    	required: true,
    	index: {
    		unique: true
    	}
    },
    ruleSta: {
        type: Boolean,
        required: true
    },
    ruleMenu: [{
        type: Schema.Types.ObjectId,
        ref: 'Menu'
    }],
    ruleModule: [{
        type: Schema.Types.ObjectId,
        ref: 'Module',
        action : [{
        	x: String,
        	lowercase: true,
        	sta: Boolean
        }]
    }],
  	dateIn: {
  		type: Date,
  		default: Date.now
  	}
});
module.exports = mongoose.model('Rule', RuleSchema);