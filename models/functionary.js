var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FuncSchema = new Schema({
    funcName: {
        type: String,
        required: true
    },
    funcEmail: {
        type: String,
        required: true
    },
    funcPhone: {
        type: String
    },
    funcNit: {
    	type: String,
    	required: true,
    	index: {
    		unique: true
    	}
    },
    funcSta: {
        type: Boolean,
        required: true
    },
  	funcCompany: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: true
  	},
    funcTrain: [{
        type: Schema.Types.ObjectId,
        ref: 'Train'
    }],
  	funcUser: {
        type: Schema.Types.ObjectId,
        ref: 'User'
  	},
    functIn: {
      type: Date,
      default: Date.now
    },
    functEnd: {
      type: Date,
      default: Date.now
    },
  	dateIn: {
  		type: Date,
  		default: Date.now
  	},
  	userIn: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
  	}
});
module.exports = mongoose.model('Functionary', FuncSchema);