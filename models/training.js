var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TrainSchema = new Schema({
    trainName: {
        type: String,
        required: true
    },
    trainSta: {
        type: Boolean,
        required: true
    },
  	trainHouer: {
        type: Number,
        required: true
  	},
  	dateIn: {
  		type: Date,
  		default: Date.now
  	},
  	userIn: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
  	}
});
module.exports = mongoose.model('Train', TrainSchema);