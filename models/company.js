var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CompanySchema = new Schema({
    companyName: {
        type: String,
        required: true
    },
    companyNit: {
    	type: String,
    	required: true,
    	index: {
    		unique: true
    	}
    },
    companyEmail: {
        type: String,
        required: true
    },
    companyPhone: {
        type: String
    },
    companyAdress: {
        type: String
    },
    companySta: {
        type: Boolean,
        required: true
    },
    companyTrain: [{
        type: Schema.Types.ObjectId,
        ref: 'Train'
    }],
  	dateIn: {
  		type: Date,
  		default: Date.now
  	},
  	userIn: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
  	}
});
module.exports = mongoose.model('Company', CompanySchema);