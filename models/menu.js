var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var MenuSchema = new Schema({
    menuName: {
    	type: String,
    	required: true,
    	index: {
    		unique: true
    	}
    },
    menuSta: {
        type: Boolean,
        required: true
    },
    menuPosition: {
        type: Number
    },
    menuRute: {
    	type: String,
    	required: true
    },
  	dateIn: {
  		type: Date,
  		default: Date.now
  	}
});
module.exports = mongoose.model('Menu', MenuSchema);